#include<stdio.h>
#include<string.h>
struct Student{
	char name[20];
	char subject[20];
	float mark;
};
int main()
{
	int no;
	printf("\nEnter no of students : ");
	scanf("%d", &no);
	struct Student st[no];
	char det[no][3];
	for(int row = 0 ; row < no ; row++)
	{
		printf("\nEnter Student Name : ");
		scanf("%s", &st[row].name);
		printf("Enter Subject : ");
		scanf("%s", &st[row].subject);
		printf("Enter Mark : ");
		scanf("%f", &st[row].mark);
	}
	printf("\n");
	printf("Name\tSubject\tMark\n");
	for(int row = 0 ; row < no ; row++)
	{
		printf("%s\t%s\t%.2f\n", st[row].name, st[row].subject, st[row].mark);
	}
	printf("\n");
	return 0;
}
